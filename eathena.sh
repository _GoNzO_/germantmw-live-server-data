#!/bin/bash

cd /home/server/eathena-data

eathena_start() {
	echo "Server: eAthena-server werden gestartet..."
	screen -d -m  ./run/login-server
	screen -d -m  ./run/char-server
	screen -d -m  ./run/map-server
}

eathena_stop() {
	echo "Server: eAthena-server werden beendet..."
	killall login-server
	killall char-server
	killall map-server
}

eathena_restart() {
	eathena_stop
	echo "Server: eAthena-server wird in kuerze gestartet..."
	sleep 1
	eathena_start
}

case "$1" in
'start')
	eathena_start
	;;
'stop')
	eathena_stop
	;;
'restart')
	eathena_restart
	;;
*)
	echo "usage $0 start|stop|restart"
esac
