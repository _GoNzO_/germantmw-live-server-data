#!/bin/sh
##funktion

while [ 1 ]; do
ATHENATXT="/home/server/eathena-data/save/athena.txt"
DEST="/var/www/stats"

cd /home/server/eathena-data/

cat save/athena.txt | sort -n > athena-sorted.txt

printf "Top verheiratete Paare\n===================\n" > ${DEST}/married.txt
cat athena-sorted.txt | ./run/marriage-info -c | sort -rn | awk '{printf "%d   %s  oo  %s  (%s (%s, %d), %s(%s, %d))\n", ++rank, $3, $6, $3, $4, $5, $6, $7, $8}' | tr '%' ' ' >> ${DEST}/married.txt

printf "Top weibliche Singles\n==================\n" > ${DEST}/female-singles.txt
cat athena-sorted.txt | ./run/marriage-info -f | sort -rn | awk '{printf "%d   %s(%d)\n", ++rank, $3, $1}' | tr '%' ' ' | head -50 >> ${DEST}/female-singles.txt

printf "Top männliche Singles\n===============\n" > ${DEST}/male-singles.txt
cat athena-sorted.txt | ./run/marriage-info -m | sort -rn | awk '{printf "%d   %s(%d)\n", ++rank, $3, $1}' | tr '%' ' ' | head -50 >> ${DEST}/male-singles.txt

printf "Top Singles\n===============\n" > ${DEST}/singles.txt
cat athena-sorted.txt | ./run/marriage-info -s | sort -rn | awk '{printf "%d   %s(%s, %d)\n", ++rank, $3, $4, $1}' | tr '%' ' ' | head -50 >> ${DEST}/singles.txt

rm athena-sorted.txt

##stats

:> ${DEST}/top-money.txt
(echo "TOP 50 > Reichsten Spieler"; echo ""; cat ${ATHENATXT} \
| awk -F , '{sub(/^[0-9] */,"",$2);sub(/ *[0-9]$/,"",$2); sub(/ *[0-9]+$/,"",$6);print $6,$2}' \
| sort -nr | head -n 50 | awk '{first=$1;$1="";print $0, "("first")"}' | nl -s ". "; echo ""; echo "Generated at `date`") >> ${DEST}/top-money.txt

:> ${DEST}/top-highest-level.txt
(echo "TOP 50 > höchste Spieler Level"; echo ""; cat ${ATHENATXT} \
| awk -F , '{sub(/^[0-9] */,"",$2);sub(/ *[0-9]$/,"",$2); print $3,$2}' \
| sort -nr | head -n 50 | awk '{first=$1;$1="";print $0, "("first")"}' | nl -s ". "; echo ""; echo "Generated at `date`") >> ${DEST}/top-highest-level.txt

:> ${DEST}/top-highest-str.txt
(echo "TOP 50 > höchste Stärke"; echo ""; cat ${ATHENATXT} \
| awk -F , '{sub(/^[0-9] */,"",$2);sub(/ *[0-9]$/,"",$2); sub(/^[0-9]+[[:space:]]*/,"",$9);print $9,$2}' \
| sort -nr | head -n 50 | awk '{first=$1;$1="";print $0, "("first")"}' | nl -s ". "; echo ""; echo "Generated at `date`") >> ${DEST}/top-highest-str.txt

:> ${DEST}/top-highest-agi.txt
(echo "TOP 50 > höchste Beweglichkeit"; echo ""; cat ${ATHENATXT} \
| awk -F , '{sub(/^[0-9] */,"",$2);sub(/ *[0-9]$/,"",$2); print $10,$2}' \
| sort -nr | head -n 50 | awk '{first=$1;$1="";print $0, "("first")"}' | nl -s ". "; echo ""; echo "Generated at `date`") >> ${DEST}/top-highest-agi.txt

:> ${DEST}/top-highest-vit.txt
(echo "TOP 50 > höchste Gesundheit"; echo ""; cat ${ATHENATXT} \
| awk -F , '{sub(/^[0-9] */,"",$2);sub(/ *[0-9]$/,"",$2); print $11,$2}' \
| sort -nr | head -n 50 | awk '{first=$1;$1="";print $0, "("first")"}' | nl -s ". "; echo ""; echo "Generated at `date`") >> ${DEST}/top-highest-vit.txt

:> ${DEST}/top-highest-int.txt
(echo "TOP 50 > höchste Inteligenz"; echo ""; cat ${ATHENATXT} \
| awk -F , '{sub(/^[0-9] */,"",$2);sub(/ *[0-9]$/,"",$2); print $12,$2}' \
| sort -nr | head -n 50 | awk '{first=$1;$1="";print $0, "("first")"}' | nl -s ". "; echo ""; echo "Generated at `date`") >> ${DEST}/top-highest-int.txt

:> ${DEST}/top-highest-dex.txt
(echo "TOP 50 > höchste Ausdauer"; echo ""; cat ${ATHENATXT} \
| awk -F , '{sub(/^[0-9] */,"",$2);sub(/ *[0-9]$/,"",$2); print $13,$2}' \
| sort -nr | head -n 50 | awk '{first=$1;$1="";print $0, "("first")"}' | nl -s ". "; echo ""; echo "Generated at `date`") >> ${DEST}/top-highest-dex.txt

:> ${DEST}/top-highest-luk.txt
(echo "TOP 50 > höchstes Glück"; echo ""; cat ${ATHENATXT} \
| awk -F , '{sub(/^[0-9] */,"",$2);sub(/ *[0-9]$/,"",$2); sub(/[[:space:]]*[0-9]+$/,"",$14);print $14,$2}' \
| sort -nr | head -n 50 | awk '{first=$1;$1="";print $0, "("first")"}' | nl -s ". "; echo ""; echo "Generated at `date`") >> ${DEST}/top-highest-luk.txt

sleep 3600;
done