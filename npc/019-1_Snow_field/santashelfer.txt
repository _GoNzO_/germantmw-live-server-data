019-1.gat,74,75,0	script	Santa's Helper	105,{
	if (ChristmasQuest2 == 1) goto L_Done;
	mes "[Santas Helfer]";
	mes "\"Hallo mein kleiner Freund.";
	mes "Wuerdest du mir bitte helfen?\"";
	next;
	mes "[Santas Helfer]";
	mes "\"Diese Monster da, haben Santas Geschenke gestohlen.";
	mes "Wenn du mir hilfst, werde ich dir etwas tolles dafuer geben.\"";
	next;

L_M:
	menu
		"Ich habe einige Geschenkboxen", L_Y,
		"Hmm, wir sehen uns spaeter", L_N,
		"Wieviel brauchst du denn genau?", L_R;

L_Y:
	mes "[Santas Helfer]";
	mes "\"Hmm, zeig mir mal was du so hast.\"";
	next;
	if (countitem("PurplePresentBox") < 25) goto L_NoItem;
	if (countitem("BluePresentBox") < 20) goto L_NoItem;
	if (countitem("GreenPresentBox") < 5) goto L_NoItem;
	getinventorylist;
	if (@inventorylist_count > 99) goto L_TooMany;
	mes "[Santas Helfer]";
	mes "\"Wunderbar! Hier habe ich etwas fuer dich\"";
	delitem "PurplePresentBox", 25;
	delitem "BluePresentBox", 20;
	delitem "GreenPresentBox", 5;
	getitem "TurtleneckSweater", 1;
	set ChristmasQuest2,1;
	close;

L_R:
	mes "[Santas Helfer]";
	mes "\"Ich brauche:";
	mes "25 [Lilanes Geschenk]";
	mes "20 [Blaues Geschenk]";
	mes "5 [Gruenes Geschenk]\"";
	next;
	goto L_M;

L_N:
	mes "[Santas Helfer]";
	mes "\"Geniess deine Ferien und frohe Weihnachten!\"";
	close;

L_NoItem:
	mes "[Santas Helfer]";
	mes "\"Es scheint so, als haettest du nicht genug Geschenke.\"";
	close;

L_Done:
        mes "[Santas Helfer]";
	mes "\"Alle Kinder werden ihre Geschenke bekommen und Weihnachten ist gerettet.";
        mes "Vielen Dank fuer deine Hilfe.\"";
        close;

L_TooMany:
        mes "[Santas Helfer]";
	mes "\"Es sieht so aus als haettest du keinen Platz mehr. Komm spaeter wieder wenn du etwas Platz gemacht hast.\"";
        close;
}
