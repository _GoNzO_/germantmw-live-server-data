﻿// A professor at Tulimshar's magic academy

024-1.gat,83,51,0	script	[Tondar]	168,{
	mes "[Tondar]";
	mes "\"Wir akzeptieren momentan keine Studenten.\"";
        if (getskilllv(SKILL_MAGIC) > 0)
		goto L_may_ask;

	close;

L_may_ask:
        next;
        menu
		"Kannst du mir einen Magie beibringen?", L_askspell,
                "Habt ihr Magischen Unterricht?", L_library,
				"Bist du sicher??  Ich kann gut bezahlen...", L_nopay,
                "Sehr schade.", -,
                "Auf Wieder sehn.", -;

	close;

L_library:
	mes "[Tondar]";
	mes "\"Ja, sicher haben wir.  Es ist nur für Graduate Studenten.\"";
        next;
        menu
		"Danke und auf wiedersehn.", L_end,
		"Niemand anderes ist erlaubt?", -;

	mes "[Tondar]";
	mes "\"Gut, es kann sein das man vom Meister akzeptiert wird.  Aber er ist momentan nicht da.\"";
        close;

L_askspell:
	mes "[Tondar]";
	mes "\"Wir bringen nur Studenten Magie bei.\"";
        next;

        menu
				"Komm schon, nur ein kleiner Spruch!", L_nopay,
                "Bitte", L_spell,
				"Ich kann dich auch bezahlen...", L_nopay;

L_spell:
	mes "[Tondar]";
	mes "\"Gut, okay... Überredet Dieser Spruch kann ja nicht so schlim sein...  Drück deinen Hände zusammen und sag `" + getspellinvocation("ask-magic-exp") + "'.\"";
        mes "\"somit weißt du wie weit du mit deiner Magie bist.  Versuchs doch mal.\"";
        close;

L_nopay:
	mes "[Tondar]";
        mes "Tondar sieht Wütend aus.";
	mes "\"Nein, ich werde dir nichts beibringen.\"";

L_end:
        close;
}
