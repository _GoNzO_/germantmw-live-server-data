function	script	QuestTreeTrigger	{
        set @Q_MASK, NIBBLE_2_MASK;
        set @Q_SHIFT, NIBBLE_2_SHIFT;

        set @Q_status, (QUEST_MAGIC & @Q_MASK) >> @Q_SHIFT;
        set @Q_status_lower, @Q_status & 3;
        set @Q_status, (@Q_status & 12) >> 2;

        if (@Q_status & @flag)
		close;  // already did that

        if (@flag == 2)
		goto L_hug;

L_cont:
        set @Q_status, @Q_status | @flag;
        callsub S_update_var;

        if (@Q_status != 3)
        	close;

        if (MAGIC_FLAGS & MFLAG_DID_CUTTREE)
		close;

	mes "Vielleicht ist es nur eine optische Täuschung, aber der Baum sieht anders aus... gesünder, fast jünger.";
        mes "[20000 experience points]";
        getexp 20000, 0;
        set @value, 15;
        callfunc "QuestSagathaHappy";
        close;

L_hug:
	mes "Du fällst den Baum.";
        next;
        goto L_cont;

S_update_var:
        set @Q_wr_status, (@Q_status << 2) | @Q_status_lower;
	set QUEST_MAGIC,
		(QUEST_MAGIC & ~(@Q_MASK)
		| (@Q_wr_status << @Q_SHIFT));
	return;
}

function	script	QuestTreeTouch	{
        set @Q_MASK, NIBBLE_2_MASK;
        set @Q_SHIFT, NIBBLE_2_SHIFT;

        set @Q_status, (QUEST_MAGIC & @Q_MASK) >> @Q_SHIFT;
        set @Q_status, (@Q_status & 12) >> 2;

        if (@Q_status == 3)
		goto L_happy;

        if ((MAGIC_FLAGS & (MFLAG_KNOWS_DRUIDTREE | MFLAG_KNOWS_CUTTREE)) == MFLAG_KNOWS_CUTTREE)
		goto L_cut;

        if ((MAGIC_FLAGS & (MFLAG_KNOWS_DRUIDTREE | MFLAG_KNOWS_CUTTREE)) == MFLAG_KNOWS_DRUIDTREE)
		goto L_water;

        if ((MAGIC_FLAGS & (MFLAG_KNOWS_DRUIDTREE | MFLAG_KNOWS_CUTTREE)) > 0)   //i.e., both are set
		goto L_both;

	mes "[Sterbender Baum]";
	mes "Du siehst einen merkwürdigen Baum.";
	close;

L_cut:
	mes "[Sterbender Baum]";
        mes "Das muss der Baum sein, den der Erdgeist meinte.";
        next;
        menu
		"Schneide einen Ast ab.", L_do_cut,
		"Lass' ihn alleine.", -;
        close;

L_water:
	mes "[Sterbender Baum]";
        mes "Das muss der Druidenbaum sein.";
        next;
        menu
		"Gieße den Baum.", L_givewater,
		"Küsse dem Baum.", L_kiss, 
		"Lass' ihn alleine.", -;
        close;

L_both:
	mes "[Sterbender Baum]";
        mes "Das muss der Druidenbaum sein, über den Wyara und der Erdgeist gesprochen haben.";
        next;
        menu
		"Gieße den Baum.", L_givewater,
		"Küsse den Baum.", L_kiss, 
		"Schneide einen Ast ab.", L_do_cut,
		"Lass' ihn alleine.", -;
        close;

L_givewater:
        if (countitem("BottleOfWater") < 1)
		goto L_no_water;
        delitem "BottleOfWater", 1;
        getitem "EmptyBottle", 1;

	mes "[Sterbender Baum]";
        mes "Du schüttest einen Eimer Wasser in den Sand. Das Wasser versickert schnell, ohne Wirkung.";
        close;

L_no_water:
	mes "[Sterbender Baum]";
        mes "Du hast kein Wasser.";
        close;

L_kiss:
	mes "[Sterbender Baum]";
        mes "Du zupfst einen Holzsplitter aus deiner Lippe.";
        mes "Irgendwie glaubst du nicht, dass es half.";
        close;

L_do_cut:
        if (countitem(570) < 1)
		goto L_no_boneknife;

	if (MAGIC_FLAGS & MFLAG_DID_CUTTREE)
		goto L_really_cut;

	mes "[Sterbender Baum]";
        mes "Du findest einen brauchbaren Ast und bringst dein Knochenmesser in Position.";
        mes "Während du den Ast hälst, fühlst du dich unbehaglich - als wenn etwas im Baum versucht sich dir zu wiedersetzen...";
        mes "Möchtest du weitermachen?";
        next;

        menu
		"Nee... lieber nicht.", -,
		"Ja, klar!", L_really_cut;
	close;

L_really_cut:
        set MAGIC_FLAGS, MAGIC_FLAGS | MFLAG_DID_CUTTREE;
        getitem "DruidTreeBranch", 1;
	mes "[Sterbender Baum]";
        mes "Du schneidest einen Ast vom Baum ab.";
        mes "Plötzlich fühlst du dich unbehaglich, als wenn der Ast sich drehen und winden würde in deiner Hand...";
        close;

L_no_boneknife:
	mes "[Sterbender Baum]";
	mes "Egal wie du es versuchst, du schaffst es nicht, den Ast abzusägen. Möglicherweise brauchst du ein anderes Werkzeug?";
        close;

L_happy:
	mes "[Druidenbaum]";
	mes "Der Baum sieht nun jünger und gesünder aus.";
	close;
}

005-1.gat,79,41,0	script	#DruidTree0#_M	127,{
	callfunc "QuestTreeTouch";
}

005-1.gat,80,41,0	script	#DruidTree1#_M	127,{
	callfunc "QuestTreeTouch";
}
