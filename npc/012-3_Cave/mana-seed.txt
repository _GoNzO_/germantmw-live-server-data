// Map 012-3 Mana Cave - Mana Seed

012-3.gat,62,130,0	script	[Mana Seed]#MAGIC	166,{
	setarray @magic_exp_needed,
		 0, // level 0
		 0,
		 100,
		 1200,
		 8000,
		 40000; // level 5
	setarray @exp_bonus,
		0,
		1000, // level 1
		10000,
		100000,
		400000,
		2000000; // level 5
	setarray @min_level,
		 0,
		 10,
		 30,
		 50,
		 65,
		 80; // level 5
	set @visions_nr, 12;
	setarray @visions$,
		 "Du siehst einen dunklen, unterirdischen Tempel; Du gehst auf die Knie und betest für die Sicherheit unseres Planeten. Du fängst an zu lächeln als du bemerkst, das ein Freund dich in der Dunkelheit begleitet - Doch plötzlich fällt ein Schatten über dich, und du fühlst einen stechenden Schmerz...",
		 "Es ist Mitternacht; Du bist in einer unterirdischen Höhle, bei den Wüsten-Ruinen. Langsam, während dem Halbschlaf, läufst du einen spiralförmigen Weg hoch, genau auf das Zentrum einer alten Vase. Du streckst deine Hand aus um sie zu berühren...",
		 "Das alte Schloss stinkt nach Schimmel und Sumpfgas. Es ist seit Jahrhunderten nicht benutzt worden, aber du warst schon vorher mal hier, als die Welt noch jung war. Du drehst zu deinen Begleitern um - aber irgendwas stimmt nicht mit ihnen; eine antike Kraft hat von ihnen Besitz ergriffen...",
		 "Als du das Horn an deine Lippen presst, fühlst du einen Mysteriösen, sich sammelnden und kondensierenden Wind in deiner Brust. Bald kannst du es nicht mehr kontrollieren; Der Sturm bläst dir in den Mund und wird zu einem ohrenbetäubenden Geräusch. Die schwarze Arena um dich herum beginnt einzustürzen...",
		 "Du bist allein in deinem Kopf, abgesehen von dem Diadem eines lachenden Mannes. Voller Stärke, schiesst Feuer aus deinen Händen! Du erzeugst Flamme für Flamme. Das ist das einäschern der Unschuldigen, wegen den Experimenten dieses Mannes...",
		 "Das Dorf liegt villeicht schon in Ruinen, aber an dem Ort musst du aufgewachsen sein. Die Gravuren auf den Wänden - Bilder von mächtigen Bestien - seltsamerweise sehen sie vertraut aus und du kommst nach was die Wörter bedeuten sollen. Wörter über dich, über deine Wünsche und deine Gebete... Du bist Zuhause...",
		 "Tief unter der Stadt, Der felsige Irrgarten in der Höhle verwandelt sich in einen opulenten Palast. Das muss der Ort sein, an dem die Bestie mit den vielen Augen residiert. Nervös greifst du nach der Wand; wird die Macht ihn zu vertreiben ausreichen um seine Magie auszulöschen?",
		 "Am Rande des Abgrunds öffnnet sich dir eine unendliche Tiefe. Du hast dich bewährt und bist würdig, dass sich dir die Antike offenbart...",
		 "Du bist villeicht nur ein Kind, aber du erkennst das es dieser Mann ist, der die Welt retten kann. Als plötzlich Wände auf dich zufahren, um deine kleine Gruppe zu zerquetschen, tauschst du einen flüchtigen Blick mit deiner Zwillingsschwester aus - Es gibt keinen Zweifel was du tun must...",
		 "Der heilige Platz ist von Nichts umgeben; Da ist nichts für deine Magie, du würdest keine Hoffnung zum zurückkehren haben. Der alte und doch zugleich junge Mann steht neben dir; Er hat Jahrhunderte lang auf dich gewartet. Nach alldem hat er alle Zeit der Welt...",
		 "Nichts bleibt dahinter. Das unterirdische Schloss ist jetzt leer. Geplünderte Räume und ein zerstörter Thron. Vor Angst schaudernd gehst du die Treppe hinunter, direkt zu der klagenden Unterwelt, die auf dich wartet...",
		 "Du fühlst etwas weiches. Ein Fluffy Fell berührt deine Haut, gefüllt mit Freude. Irgendwie fällt dir das Wort " + getspellinvocation("happy-curse") + " ein...";

	set @max_magic, 2;

	set @has_magic, getskilllv(SKILL_MAGIC);
	set @drank_potion, MAGIC_FLAGS & MFLAG_DRANK_POTION;
	set @knows_seed, MAGIC_FLAGS & MFLAG_KNOWS_MANASEED;

	// Set up SkillUp function
	set @SUP_id, SKILL_MAGIC;
	set @SUP_name$, "Magic";

	if (@has_magic)
		goto L_magic_start;

	if (@knows_seed)
		goto L_quick_nomagic;

	// first time here

	set MAGIC_FLAGS, MAGIC_FLAGS | MFLAG_KNOWS_MANASEED;

	mes "[Mana Samen]";
	mes "Du siehst eine glühende Kugel, die wie eine Seerose im Wasser schwimmt.";
	mes "Das überirdische Glühen, beleuchtet die ganze Höhle einschließlich dem Wasser.";
	next;

	mes "[Mana Samen]";
	if (@drank_potion)
		mes "langsam beginnt das Prickeln, das du vorher gefühlt hast, sich wieder in deinen Körper auszubreiten.";
	mes "Was willst du nun tun?";
	next;
	goto L_nomagic_mainmenu;

L_quick_nomagic:
	mes "[Mana Samen]";
	mes "Du stehst wieder vor dieser Kugel.";
	if (@drank_potion)
		mes "langsam beginnt das Prickeln, das du vorher gefühlt hast, sich wieder in deinen Körper auszubreiten.";
	mes "Was willst du nun tun?";
	next;

	goto L_nomagic_mainmenu;

// Non-Magic main menu ------------------------------------------------------------
L_nomagic_mainmenu:
	menu "Betrachte es genauer", L_nomagic_examine,
	 "Nimm es mit", L_nomagic_touch,
	 "Berühre es", L_nomagic_touch,
	 "Wirf einen Stein", L_nomagic_throwrock,
	 "Zerstöre es", L_nomagic_destroy,
	 "Lass es allein", L_end;
	close;

L_nomagic_examine:
	mes "[Mana Samen]";
	mes "Die Kugel scheint perfekt rund zu sein und leuchtet ständig. Sie scheint im Wasser zu schwimmen.";
	next;
	goto L_nomagic_mainmenu;

L_nomagic_throwrock:
	mes "[Mana Samen]";
	mes "Dein Stein macht ein lautes Geräusch als er die Kugel trifft und fällt ins Wasser, ohne das er hat einen Kratzer hinterlassen hat.";
	mes "Die Kugel bewegt sich etwas, aber dann fällt sie in die alte Position zurück, als ob sie an diesen Platz angebunden wäre.";
	next;
	goto L_nomagic_mainmenu;

L_nomagic_touch:
	mes "[Mana Samen]";
	if (@drank_potion)
		goto L_magic_level_1;
	mes "Als du die Kugel berührst, fühlst du eine schreckliche Kraft in dir, getrennt von deiner dünnsten Membrane - wie ein Gewitter wo alle in einem kleinen Wasserschleier umhüllt sind.";
	next;
	mes "[Mana Samen]";
	mes "Für einen Moment fühlst du diese Kraft durch deine Hände und dann durch deinen Körper hindurcheilen, als ob du das Gewitter selbst beschworen hättest. Durch diese unkontrollierbare Macht, fällst du rückwärts, weg von der Kugel.";
	set MAGIC_FLAGS, MAGIC_FLAGS | MFLAG_TOUCHED_MANASEED;
	next;
	goto L_nomagic_mainmenu;

L_magic_level_1:
	if (getskilllv(SKILL_MAGIC))
		goto L_end; // shouldn't be happening

	mes "Und schon wieder fühlst du das Gewitter vom Mana Samen durch deinen Körper hindurchziehen. Du schaffst es nur, es für einen Moment anzuhalten und bist gezwungen diese Kraft schnell aus deinen Haenden herauszulassen.";
	next;

	mes "[Mana Samen]";
	mes "Aber dieses Mal ist etwas anders - Diese prickelnde Kraft ist zurück und stärker als jemals zuvor. Es sprudelt nur so durch deinen Körper, von deinem Kopf bis zu den Zehen, und du fühlst dich durch die Energie wie aufgeladen.";
	next;

	mes "[Mana Samen]";
	mes "Es ist ein atemberaubendes Gefühl, aber du siehst dich gezwungen, für einen Moment auf den Boden zu sitzen, um dich von dem Erlebnis zu erholen.";
	mes "Irgendwas ist anders. Einen neue Kraft ist in dir gewachsen und wartet darauf von dir genutzt zu werden.";
	set @SUP_xp, 1000;
	set @SUP_lvl, 1;
	callfunc "SkillUp";
	next;

	goto L_end;

L_nomagic_destroy:
	mes "[Mana Samen]";
	mes "Du versuchst es zu zerstören so gut du kannst, aber deine Attacken scheinen der Kugel nichts auszumachen. Durch die Anstrengung bist du gezwungen aufzugeben.";
	next;
	goto L_nomagic_mainmenu;

// Magic main menu ------------------------------------------------------------
L_magic_start:
	mes "[Mana Samen]";
	mes "Der Mana Samen ist immer noch am selben Platz seit deinem letzten Besuch und breitet sein Licht wie gewohnt durch die ganze Höhle aus.";
	mes "Was willst du tun?";
	next;

L_magic_mainmenu:
	menu "Berühre es", L_magic_touch,
	 "Zerstöre es", L_magic_destroy,
	 "Lass es allein", L_end;
	close;

L_magic_touch:
	mes "[Mana Samen]";
	mes "Du berührst erneut den Mana Samen.";

	if (getskilllv(SKILL_MAGIC) >= @max_magic)
		goto L_magic_maxed_out;

	set @exp_needed, @magic_exp_needed[getskilllv(SKILL_MAGIC) + 1];
	set @magic_exp, MAGIC_EXPERIENCE & 65535;
	if (@magic_exp >= @exp_needed)
		goto L_magic_levelup;

	set @prev_exp_needed, @magic_exp_needed[getskilllv(SKILL_MAGIC)];
	set @exp_diff, @exp_needed - @prev_exp_needed;
	set @index, ((@magic_exp - @prev_exp_needed) * 5) / @exp_diff;
	setarray @messages$,
		"Die Energie der Kugel gleitet mühelos durch dich hindurch und ignoriert deine schwachen Versuche mehr Magie zu erhalten. Du wirst eine viel bessere Kontrolle über deine Magie benötigen bevor mehr magische Kraft bekommst.",
		"Du bringst es gerade mal fertig der Kugel ein bisschen Energie zu entnehmen, aber nicht genug um stärker zu werden. Du brauchst merklich eine bessere Kontrolle über deine Magie.",
		"Die Kräfte der Kugel sind immer noch zu stark für dich; Du wirst eine bessere Kontrolle über deine Magie benötigen um stärker zu werden.",
		"Du fühlst, das du im Stande bist der Kugel die Magie zu entnehmen. Aber du brauchst immer noch eine bessere Kontrolle über deine Magie.",
		"Die Kugel weicht kaum noch deinen Versuchen aus mehr magische Kraft zu gewinnen. Schon bald wirst du in der Lage sein die Kräfte der Kugel für dich zu gewinnen.";

	mes @messages$[@index];
	next;
	goto L_magic_mainmenu;

L_magic_levelup:
	set @baselevel_needed, @min_level[getskilllv(SKILL_MAGIC) + 1];
	if (BaseLevel < @baselevel_needed)
		goto L_insufficient_baselevel;
	mes "Die Energie der Kugel umringt dich. Du bist unsicher, ob du es bist der die Kugel kontrolliert, oder ob die Kugel plötzlich dich kontrolliert.";
	next;

	mes "[Mana Samen]";
	mes "Das Gewitter beruhigt sich in deinen Händen, und die Energie vereint sich mit deiner Energie.";
	next;

	mes "[Mana Samen]";
	mes "Ein Gefühl der Harmonie breitet sich durch deinen Körper aus, und du fühlst wieder das prickeln, inner- und ausserhalb deines Körpers.";
	next;

	mes "[Mana Samen]";
	mes "Als sich das Prickeln verstärkt, fühlst du dich ganz leicht... richtig schwerelos.";
	mes "Alles verwelkt...";
	next;

	mes "Das Prickeln und das Gefühl der Harmonie, beides ist verschwunden. Dunkelheit erfüllt den Raum. Du kannst den Mana Samen nicht mehr fühlen...";
	next;

	mes "... du fühlst nichts mehr.";
	next;

	mes "du schwebst leicht...";
	next;

	mes "Nicht alles ist dunkel. Du beginnst Sterne in der Ferne auszumachen. Sie kreisen einander ein und es sieht aus wie ein Tanz! Ein kosmischer Sternentanz.";
	next;

	mes "Bilder und Schatten eilen in deinen Träumen an dir vorbei. Gefühle kommen dabei hoch und treffen dich innerlich - Verzweiflung, manchmal verloren, manchmal Hoffung, Hass, Liebe.";
	next;

	mes "Gedanken anderer Personen teilen deine Meinung...";
	next;

	set @nr, rand(@visions_nr);
	mes @visions$[@nr];
	next;

	mes "Das Bild verwelkt.";
	next;

	mes "[Mana Samen]";
	mes "Du wachst auf und bemerkst, das du auf dem Boden liegst.";
	next;

	mes "[Mana Samen]";
	mes "Etwas hat sich geändert... Du fühlst dich stärker in deinen magischen Fähigkeiten.";

	set @SUP_xp, @exp_bonus[1 + getskilllv(SKILL_MAGIC)];
	set @SUP_lvl, 1 + getskilllv(SKILL_MAGIC);
	callfunc "SkillUp";
	itemheal 0, 10000;
	next;

	goto L_end;

L_magic_maxed_out:
	mes "Seltsamerweise fühlst du nichts, als ob der Mana Samen dich ignorieren würde.";
	set MAGIC_FLAGS, MAGIC_FLAGS | MFLAG_MANASEED_MAXEDOUT;
	next;

	goto L_magic_mainmenu;

L_insufficient_baselevel:
	mes "Energie eilt durch deinen Körper. Du kämpfst darum, sie unter Kontrolle zu kriegen und sie in deinem Körper zu speichern. Doch leider ist dein Körper zu labil - Du solltest es lieber sein lassen.";
	mes "Frustriert gibst du auf. Du hättest die Fähigkeit diese Kräfte zu kontrollieren, aber du musst noch etwas wachsen und erfahrener werden bevor dein Körper damit umgehen kann.";
	next;
	goto L_magic_mainmenu;

L_magic_destroy:
	mes "[Mana Samen]";
	mes "Du versuchst den Mana Samen zu zerstören so gut du kannst, aber du findest keinen Weg den Samen zu zerstören, sowohl physisch als auch magisch. Du gibst frustiert auf.";
	next;
	goto L_magic_mainmenu;

L_end:
	close;
}
