// Map 300-2 - Höhlendurchgang Regenwald - Mobs

300-2.gat,0,0,0,0	monster	RedSlime	1008,30,60000,30000,Mob300-2::On1008
300-2.gat,0,0,0,0	monster	BlackScorpoin	1009, 5,60000,30000,Mob300-2::On1009
300-2.gat,0,0,0,0	monster	MountainSnake	1026, 5,60000,30000,Mob300-2::On1026
300-2.gat,44,34,0,0	monster	FireSkull	1023, 1,60000,30000,Mob300-2::On1023

300-2.gat,0,0,0|script|Mob300-2|-1,{

On1008:
	set @mobID, 1008;
	callfunc "MobPoints";
	end;

On1009:
	set @mobID, 1009;
	callfunc "MobPoints";
	end;

On1026:
	set @mobID, 1026;
	callfunc "MobPoints";
	end;

On1023:
	set @mobID, 1023;
	callfunc "MobPoints";
	end;

	end;
}
