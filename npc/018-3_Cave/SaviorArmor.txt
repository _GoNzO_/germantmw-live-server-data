// savior armor @TEMP rand 3...

018-3.gat,142,34,0	script	[Truhe]	111,{
	if (baselevel < 30) goto L_zujung;
	if( SaviorArmor & 2) goto L_Finished;
	if( SaviorArmor & 1) goto L_Pants;

	mes "[Truhe]";
	mes "Möchtest du diese Truhe öffnen?";
	next;
	menu
		"Ja", L_Yes,
		"Nein", -;
	close;

L_Yes:
	if(countitem("SilverOre") < 10) goto L_Not_Enough;
	getinventorylist;
	if (@inventorylist_count == 100 && countitem("SilverOre") > 10) goto L_TooMany;
	mes "[Truhe]";
	 set @TEMP,rand(4);
		if(@TEMP == 0) goto	L_1;
		if(@TEMP == 1) goto	L_2;
		if(@TEMP == 2) goto	L_3;
		if(@TEMP == 3) goto	L_4;

//weiß
L_1:
	mes "Du öffnest sie und findest eine [Weisse Savior BrustPlatte]";
	delitem "SilverOre", 10;
	getitem 3932, 1;
	set SaviorArmor, SaviorArmor | 1;
	close;

//rot
L_2:
	mes "Du öffnest sie und findest eine [Rote Savior BrustPlatte]";
	delitem "SilverOre", 10;
	getitem 3935, 1;
	set SaviorArmor, SaviorArmor | 1;
	close;

//blau
L_3:
	mes "Du öffnest sie und findest eine [Blaue Savior BrustPlatte]";
	delitem "SilverOre", 10;
	getitem 3934, 1;
	set SaviorArmor, SaviorArmor | 1;
	close;

//schwarz
L_4:
	mes "Du öffnest sie und findest eine [Schwarze Savior BrustPlatte]";
	delitem "SilverOre", 10;
	getitem 3933, 1;
	set SaviorArmor, SaviorArmor | 1;
	close;

//////////////
//Savior-Hose
L_Pants:
	if(countitem("IronOre") < 5) goto L_Not_Enough;
	getinventorylist;
	if (@inventorylist_count == 100 && countitem("SilverOre") > 10) goto L_TooMany;
	mes "[Truhe]";
	mes "Möchtest du diese Truhe noch ein mal öffnen?";
	next;
	menu
		"Ja", L_Yes1,
		"Nein", -;
	close;

L_Yes1:
	mes "[Truhe]";
	mes "Du öffnest die Truhe und findest eine [SaviorHose]";
	delitem "IronOre", 5;
	getitem 3931, 1;
	set SaviorArmor, SaviorArmor | 2;
	close;
//////////////

//functionen
L_Not_Enough:
    mes "[Truhe]";
	mes "Du hast keine Ahnung wie du diese Truhe öffnen sollst.";
	close;

L_Finished:
    mes "[Truhe]";
    mes "Es ist absolut nix mehr darin.";
    close;

L_TooMany:
    mes "[Truhe]";
    mes "Du hast keinen Platz im Inventar, komm später wieder.";
	close;

L_zujung:
	mes "Du bist zu Jung um diese Truhe zu öffnen.";
	mes "Min-Level ist 30!";
}
