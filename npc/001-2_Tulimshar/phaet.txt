//

001-2.gat,24,23,0	script	[Phaet]	125,{
	mes "[Phaet die herrschaftliche Wache]";
	mes "\"Hey, du scheinst stark genug zu sein! Würdest du gerne deine Fähigkeiten beweisen? Ich lasse dich in die Arena, wenn du mir 50 GP gibst. Du kannst dort gegen andere Spieler kämpfen.\"";
	next;

	menu
		"Ja", L_Sure,
		"Nein", -;

	mes "[Phaet die herrschaftliche Wache]";
	mes "\"Ha ha, Feigling.\"";
	close;

L_Sure:
	if (zeny < 50) goto L_NoMoney;
	set zeny, zeny - 50;

	mes "[Phaet die herrschaftliche Wache]";
	mes "\"Dann los!\"";
	next;

	warp "001-3.gat", 0, 0;
	close;

L_NoMoney:
	mes "\"Warte eine Sekunde, du hast nicht genügend Geld.\"";
	close;
}
