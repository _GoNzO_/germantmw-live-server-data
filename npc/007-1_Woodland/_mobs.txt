// 007-1 Woodland mobs

007-1.gat,0,0,0,0	monster	Flower	1014,15,100000,30000,Mob007-1::On1014
007-1.gat,0,0,0,0	monster	Spiky Mushroom	1019,12,100000,30000,Mob007-1::On1019
007-1.gat,0,0,0,0	monster	Fluffy	1020,25,100000,30000,Mob007-1::On1020
007-1.gat,0,0,0,0	monster	Mauve	1029,3,270000,180000,Mob007-1::On1029
007-1.gat,0,0,0,0	monster	Gamboge	1031,1,2700000,1800000,Mob007-1::On1031
007-1.gat,0,0,0,0	monster	SilkWorm	1035,2,60000,30000,Mob007-1::On1035
007-1.gat,0,0,0,0	monster	Clover	1037,10,0,1000,Mob007-1::On1037
007-1.gat,0,0,0,0	monster	Squirrel	1038,20,30,20,Mob007-1::On1038
007-1.gat,0,0,0,0	monster	Biene		1062,10,30,20,Mob007-1::On1062
007-1.gat,0,0,0,0	monster	Schmetterling	1063,20,30,20,Mob007-1::On1063

007-1.gat,0,0,0	script	Mob007-1	-1,{
On1014:
	set @mobID, 1014;
	callfunc "MobPoints";
	break;

On1019:
	set @mobID, 1019;
	callfunc "MobPoints";
	break;

On1020:
	set @mobID, 1020;
	callfunc "MobPoints";
	break;

On1029:
	set @mobID, 1029;
	callfunc "MobPoints";
	break;

On1031:
	set @mobID, 1031;
	callfunc "MobPoints";
	break;

On1035:
	set @mobID, 1035;
	callfunc "MobPoints";
	break;

On1037:
	set @mobID, 1037;
	callfunc "MobPoints";
	break;

On1038:
	set @mobID, 1038;
	callfunc "MobPoints";
	break;

On1062:
	set @mobID, 1062;
	callfunc "MobPoints";
	break;

On1063:
	set @mobID, 1063;
	callfunc "MobPoints";
	break;

	end;
}
