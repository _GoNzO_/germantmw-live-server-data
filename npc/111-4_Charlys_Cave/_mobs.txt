// 111-4 H�hlen-R�tsel mobs

111-4.gat,15,54,37,37	monster	Maggot	1002,15,30000,30000,Mob111-4::On1002
111-4.gat,89,65,59,19	monster	Spider	1012,5,10,10,Mob111-4::On1012
111-4.gat,89,65,59,19	monster	Bat	1017,25,10,10,Mob111-4::On1017

111-4.gat,0,0,0	script	Mob111-4	-1,{
On1002:
	set @mobID, 1002;
	callfunc "MobPoints";
	break;
On1012:
	set @mobID, 1012;
	callfunc "MobPoints";
	break;
On1017:
	set @mobID, 1017;
	callfunc "MobPoints";
	break;
end;
}
