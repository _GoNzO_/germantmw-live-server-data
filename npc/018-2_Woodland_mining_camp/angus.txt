// Angus the craftsman

018-2.gat,31,39,0	script	Angus	147,{
	set @honorific$, "Bub";
	if (Sex == 0) set @honorific$, "Mädsche";

	if (Inspector == 10) goto L_NohMask;

	mes "Angus bastelt an einer mechanischen Erfindung herum.";
	mes "[Angus]";
	mes "\"Des brischt immer ab da, links un' reschts... zum Glück hab isch überall Redundanze eingebaut, gell? Aber isch muss des weiter repariere'.\"";
	close;

L_NohMask:
	mes "[Angus]";
	mes "\"Nein des tut mer Leid, " + @honorific$ + ", werklisch Leid, aber isch schlaf in der Stadt, weisste. Einer der Abbeider koennt vielleischt was gehört habbe.\"";
	close;
}
