// Map 970-2 - Kasinow Hotel - NPC: Skilltrainerin

970-2.gat,37,77,0|script|[SKILLTRAINERIN]|165,{
set @pvp_system, 7;
set @anbau_pflanzen, 420;
	mes "[SkillTrainer]";
	mes "Hallo, ich bin der SkillTrainer,";
	mes "und wer bist du?";
	menu
		"Mein name ist " + strcharinfo(0) + " .",L_name,
		"Uninteresant...",-;
	close;
L_name:
	mes "[SkillTrainer]";
	mes "Hallo "+ strcharinfo(0);
	mes "Was führt dich zu mir?";
	menu
		"Ich möchte gegen andere Spieler kämpfen",L_fight,
		"Ich möchte eine Gilde erstellen",L_guild,
		"Kann ich selbst Pflanzen züchten?",L_plants,
		"Nichts",-;
	close;
L_fight:
	mes "Ich hoffe nur wenn diese das auch wollen!";
	menu
		"Natürlich",L_fight_yes,
		"Ich bin ein Killer",-;
	close;
L_fight_yes:
	if (getskilllv (@pvp_system) >= 1) goto L_fight_already;
	mes "Oh gut, wen das so ist...";
	mes "Merke dir diesen Spruch sehr gut:";
	setskill @pvp_system, 1;
	mes "\" #pvp \"";
	next;
	mes "Achte darauf das du dein pvp so auch wieder ausschalten solltest, da du somit sterblich gegen jeden Spieler bist!";
	close;
L_guild:
	mes "Das [Gilden-System] ist momentan noch im Aufbau";
	mes "Deshalb können Gilden \"momentan\" nur von Administratoren erstellt werden.";
	menu
		"Wann wird das System denn fertig sein?",L_guild_sys,
		"Nix weiter, Danke!",-;
L_guild_sys:
	mes "Das System ist wie gesagt noch im Aufbau, deshalb sind einige Sachen in den Gilden noch nicht verwendbar.";
//	mes "Aber dennoch wird das System vorraussichtich bis Weihnachten dieses Jahr fertig.";
	close;
L_plants:
	if (getskilllv (@anbau_pflanzen) >= 1) goto L_plants_already;
	mes "Können, das kommt darauf an wie oft du dies tust.";
	mes "Um so öfter du eine Pflanze anbaust... um so mehr Pflanzlvl bekommst du im Alchemie-Menü";
	mes "hiermit bekommst du dein erstes Level im Anpflanzen von Herbs.";
	setskill @anbau_pflanzen, 1;
	close;
L_plants_already:
	mes "Du hast bereits deinen Pflanzenskill, womit du Pflanzen anbauen kannst.";
	close;
L_fight_already:
	mes "Mehr als PvP geht leider nicht^^";
	mes "der Spruch ist:";
	mes "\"#pvp\"";
	close;
}
