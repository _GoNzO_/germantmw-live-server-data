// Map 015-1 South-West Woodland - Mobs

015-1.gat,0,0,0,0	monster	LogHead		1025,40,0,0,Mob015-1::On1025
015-1.gat,0,0,0,0	monster	Flower		1014,20,0,0,Mob015-1::On1014
015-1.gat,0,0,0,0	monster	Gamboge		1031,6,0,0,Mob015-1::On1031
015-1.gat,0,0,0,0	monster	Alizarin	1032,6,0,0,Mob015-1::OnA1032
015-1.gat,0,0,0,0	monster	Mauve		1029,6,270000,180000,Mob015-1::On1029
015-1.gat,0,0,0,0	monster	SilkWorm	1035,7,15000,7000,Mob015-1::On1035
015-1.gat,0,0,0,0	monster	Squirrel	1038,30,20,10,Mob015-1::On1038

015-1.gat,0,0,0|script|Mob015-1|-1,{
On1025:
	set @mobID, 1025;
	callfunc "MobPoints";
	end;

OnFlower:
	set @mobID, 1014;
	callfunc "MobPoints";
	end;

On1029:
	set @mobID, 1029;
	callfunc "MobPoints";
	end;

On1031:
	set @mobID, 1031;
	callfunc "MobPoints";
	end;

On1032:
	set @mobID, 1032;
	callfunc "MobPoints";
	end;

On1035:
	set @mobID, 1035;
	callfunc "MobPoints";
	end;

On1038:
	set @mobID, 1038;
	callfunc "MobPoints";
	end;

	end;
}
